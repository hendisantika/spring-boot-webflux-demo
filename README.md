# Spring Boot WebFlux Demo

The reactive-stack web framework, Spring WebFlux, has been added Spring 5.0. It is fully non-blocking, supports reactive
streams back pressure, and runs on such servers as Netty, Undertow, and Servlet 3.1+ containers. In this spring webflux
tutorial, we will learn the basic concepts behind reactive programming, webflux apis and a fully functional hello world
example.

Reactive programming is a programming paradigm that promotes an asynchronous, non-blocking, event-driven approach to
data processing. Reactive programming involves modeling data and events as observable data streams and implementing data
processing routines to react to the changes in those streams.

### Why Use Spring WebFlux?

Spring WebFlux will allow you to more efficiently leverage CPU and network resources, provide a more scalable
architecture and a more responsive user experience.

### From the Spring WebFlux Docs

> Why was Spring WebFlux created?
>
> Part of the answer is the need for a non-blocking web stack to handle concurrency with a small number of threads and scale with fewer hardware resources.
>
> That is important because of servers such as Netty that are well-established in the async, non-blocking space.
>
> …
>
> The other part of the answer is functional programming.
>
> The addition of lambda expressions in Java 8 created opportunities for functional APIs in Java. This is a boon for non-blocking applications and continuation-style APIs that allow declarative composition of asynchronous logic.

### Spring WebFlux Framework

Spring WebFlux is built on Project Reactor which implements the Reactive Streams specification.

From the Reactive Streams docs:
> Reactive Streams is an initiative to provide a standard for asynchronous stream processing with non-blocking back pressure.
> This encompasses efforts aimed at runtime environments (JVM and JavaScript) as well as network protocols.

WebFlux provides support for two paradigms:

1. Annotation-based Spring Controllers(Similar to SpringMVC)
2. Functional Endpoints that allow for functional, fluent API style routing and handler functions

Mono: a Reactive Publisher that emits 1 or zero elements than terminates.

In other words, a Mono is like an async, promise or a future that will emit an element.

**From the Spring JavaDocs for the Mono type**:
> A Reactive Streams Publisher with basic rx operators that emits at most one item via the onNext signal then terminates with an onComplete signal (successful Mono, with or without value), or only emits a single onError signal (failed Mono).


